<?php
namespace Drupal\hero_icons\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'hero_icons_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "hero_icons_formatter",
 *   label = "Hero Icons",
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class HeroIconsFormatter extends FormatterBase {

	/**
	 * {@inheritdoc}
	 */
	public static function defaultSettings() {
		return [
				'inline' => TRUE,
				'css_classes' => 'w-5 h-5',
				'enable_alt' => TRUE,
				'alt_string' => '',
				'enable_title' => FALSE,
				'title_string' => '',
			] + parent::defaultSettings();
	}


	/**
	 * {@inheritdoc}
	 */
	public function settingsForm( array $form, FormStateInterface $form_state ) {
		$form = parent::settingsForm($form, $form_state);

		$form['css_classes'] = [
			'#type' => 'textfield',
			'#title' => $this->t('CSS Classes'),
			'#description' => $this->t('CSS classes to be applied to SVG element' ),
			'#default_value' => $this->getSetting('css_classes')
		];



		return $form;
	}


	/**
	 * {@inheritdoc}
	 */
	public function settingsSummary() {
		$summary = [];

		// Implement settings summary.
		if ($this->getSetting('css_classes')) {
			$summary[] = $this->t('CSS classes: classes to be applied to SVG element');
		}

		// $image_style = $this->getSetting('image_style');
		// $summary[] = $this->t('Image style: @image_style', ['@image_style' => $image_style]);

		return $summary;
	}


	/**
	 * {@inheritdoc}
	 */
	public function viewElements( FieldItemListInterface $items, $langcode ) {
		$elements = [];

		foreach( $items as $delta => $item ) {
			$attributes = [];
			$image = $item->entity;
			if(empty($image)) {
				continue;
			}

			$svg_file_contents = file_exists($image->getFileUri()) ? file_get_contents($image->getFileUri()) : NULL;
			if ($image->getMimeType() !== 'image/svg+xml' && $svg_file_contents!==null) {
				$elements[ $delta ] = [
					'#theme'      => 'image',
					'#inline'     => FALSE,
					'#uri'        => $image->getFileUri(),
					'#alt'        => $image->alt,
					'#title'      => $image->title,
					'#attributes' => [
						'class' => [ '' ],
					],
					'#svg_data' => null,
				];
				continue;
			}

			// Skip if this is not a SVG image.
			if ($image->getMimeType() !== 'image/svg+xml' || $svg_file_contents===null) {
				continue;
			}

			$dom = new \DOMDocument();
			libxml_use_internal_errors(TRUE);
			$dom->loadXML($svg_file_contents);
			$cssClasses = 'hero-icon '.$this->getSetting('css_classes');
			if ($cssClasses && isset($dom->documentElement)) {
				$dom->documentElement->setAttribute('class', $cssClasses);
			}
			if (!empty($image->alt) && isset($dom->documentElement)) {
				$title = $dom->createElement('title', $image->alt);
				$title_id = uniqid();
				$title->setAttribute('id', $title_id);
				$dom->documentElement->insertBefore($title, $dom->documentElement->firstChild);
				$dom->documentElement->setAttribute('aria-labelledby', $title_id);
			}
			$svg_data = $dom->saveXML($dom->documentElement);

			$elements[$delta] = [
				'#theme' => 'hero_icons_formatter',
				'#inline' => TRUE,
				'#alt'        => $image->alt,
				'#title'      => $image->title,
				'#uri' => $image->getFileUri(),
				'#svg_data' => $svg_data,
			];

		}

		return $elements;
	}

}
